module Lib.TimeMachine where

  data TimeMachineFeature = TimeMachineFeature {
    travelToPast :: Bool,
    travelToFuture :: Bool
  } deriving Show

  type Price = Float

  data TimeMachine = TimeMachine {
    manufacturer :: String,
    model :: Integer,
    name :: String,
    feature :: TimeMachineFeature,
    price :: Price
  } deriving Show