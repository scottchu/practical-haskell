{-# LANGUAGE NamedFieldPuns #-}

module Chapter2.PersonR where

  import Data.Char

  data PersonR = PersonR { firstName :: String
                         , lastName :: String
                         } deriving Show

  -- nameInCapitals :: PersonR -> PersonR
  -- nameInCapitals p@(PersonR { firstName = "" }) = p
  -- nameInCapitals p@(PersonR { firstName = initial:rest }) = 
  --   let newName = (toUpper initial):rest
  --   in p { firstName = newName }
  nameInCapitals :: PersonR -> PersonR
  nameInCapitals person = person { firstName = (capitalize (firstName person)) }

  capitalize :: String -> String
  capitalize "" = ""
  capitalize (initial:rest) = (toUpper initial):rest

