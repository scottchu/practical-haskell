module Chapter2.SimpleFunctions where

firstOrEmpty :: [[Char]] -> [Char]
firstOrEmpty lst = if not (null lst) then head lst else "empty"

(+++) :: [a] -> [a] -> [a]
-- if else version
-- list1 +++ list2 = if null list1 {- check emptyness -}
--   then list2 -- base case
--   else (head list1) : (tail list1 +++ list2)

-- pattern matching version
list1 +++ list2 = case list1 of
  [] -> list2
  x:xs -> x:(xs +++ list2)

reverse2 :: [a] -> [a]
-- if else version
-- reverse2 list = if null list
--   then []
--   else reverse2 (tail list) +++ [head list]

-- pattern matching version
reverse2 list = case list of
  [] -> []
  x:xs -> reverse2 (xs) +++ [x]

maximum2 :: Ord a => [a] -> (a, a)
-- if else version
maximum2 list = if null (tail list)
  then (head list, head list)
  else (
    if (head list) > fst(maximum2 (tail list)) 
      then head list
      else fst (maximum2 (tail list))
    , if (head list) < snd (maximum2 (tail list))
      then head list
      else snd (maximum2 (tail list))
  )

maximum3 :: Ord a => [a] -> (a, a)
maximum3 list = let h = head list
  in if null (tail list)
  then (h, h)
  else ( if h > t_max then h else t_max
       , if h < t_min then h else t_min )
       where t = maximum3 (tail list)
             t_max = fst t
             t_min = snd t

maximum4 :: Ord a => [a] -> (a, a)
maximum4 [x] = (x, x)
maximum4 (x:xs) = (
  if x > xs_max then x else xs_max,
  if x < xs_min then x else xs_min
  ) where (xs_max, xs_min) = maximum4 xs