module Chapter2.Fibonacci where
  
  fibonacci :: Int -> Int
  fibonacci n = case n of
    0 -> 0
    1 -> 1
    _ -> fibonacci (n - 1) + fibonacci (n - 2)

  ifibonacci :: Integer -> Maybe Integer
  ifibonacci n = if n < 0
    then Nothing
    else case n of
      0 -> Just 0
      1 -> Just 1
      n' -> let Just f1 = ifibonacci(n' - 1)
                Just f2 = ifibonacci(n' - 1)
            in Just (f1 + f2)
      
  ifibonacci2 :: Integer -> Maybe Integer
  ifibonacci2 n | n < 0 = Nothing
  ifibonacci2 0 = Just 0
  ifibonacci2 1 = Just 1
  ifibonacci2 n | otherwise = let Just f1 = ifibonacci2 (n - 1)
                                  Just f2 = ifibonacci2 (n - 2)
                              in Just (f1 + f2)