module Chapter2.Sorted where

  sorted :: [Integer] -> Bool
  sorted [] = True
  sorted [_] = True
  sorted (x: z@(y:_)) = x < y && sorted(z)