{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}

module Chapter2.ClientR where

  import Chapter2.PersonR

  data ClientR = GovOrgR  { clientRName :: String }
               | CompanyR { clientRName :: String 
                          , id :: Integer
                          , person :: PersonR
                          , duty :: String }
               | IndividualR { person :: PersonR }
               deriving Show

  greet :: ClientR -> String
  greet IndividualR { person = PersonR { .. } } = "Hi, " ++ firstName 
  greet CompanyR { .. } = "Hi, " ++ clientRName
  greet GovOrgR { } = "Welcome"