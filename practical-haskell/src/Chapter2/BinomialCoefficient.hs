module Chapter2.BinomialCoefficient where

  binom _ 0           = 1
  binom x y | x == y  = 1
  binom n k           = (binom (n -1) (k - 1)) + (binom (n - 1) k)