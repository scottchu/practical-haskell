module Chapter2.Gender where

  data Gender = Male | Female | Unknown
              deriving Show