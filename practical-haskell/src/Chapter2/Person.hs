module Chapter2.Person where

  import Chapter2.Gender

  data Person = Person String String Gender
              deriving Show