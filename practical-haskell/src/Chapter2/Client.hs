{-# LANGUAGE ViewPatterns #-}
module Chapter2.Client where

  import Chapter2.Person

  data Client = GovOrg      String
              | Company     String Integer Person String
              | Individual  Person Bool
              deriving Show

  clientName :: Client -> String
  clientName client = case client of
                      GovOrg name -> name
                      Company name _ _ _ -> name
                      Individual (Person fName lName _) _ads -> fName ++ " " ++ lName

  companyName :: Client -> Maybe String
  companyName client = case client of
    Company name _ _ _ -> Just name
    _ -> Nothing

  responsibility :: Client -> String
  responsibility (Company _ _ _ r)  = r
  responsibility _                  = "Unknown"

  specialClient :: Client -> Bool
  specialClient (clientName -> "Mr. Alejandro") = True
  specialClient (responsibility -> "Director") = True
  specialClient _ = False